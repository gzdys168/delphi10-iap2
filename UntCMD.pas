unit UntCMD;

interface

uses
  SysUtils;

const
  COM_REPEAT_CNT = 3;
  COM_SPACE_DELAY = 100;
  COM_READ_DELAY = 100;
  COM_BUFF_SIZE = 255;

function start_iap_cmd(sNo: AnsiString): boolean; // 启动IAP

function get_soft_version(sNo: AnsiString; var ver: string): boolean; // 读主板固件版本

function iap_erase_cmd(sNo: AnsiString): boolean; // 发送擦除命令
function down_data_cmd(sNo: AnsiString; A: array of AnsiChar; N: integer)
  : boolean; // 下载数据
function start_app_cmd(sNo: AnsiString): boolean; // 启动App

function com_send(sValue: AnsiString; var rstC, rstS: AnsiString): boolean;

var
  rdCount: integer;
  wbuf: array [0 .. COM_BUFF_SIZE] of AnsiChar;
  rbuf: array [0 .. COM_BUFF_SIZE] of AnsiChar;

implementation

uses DYSCOM, DYSChar, UntCom, UntConst;

// 自调整动速率-----------------------------------------------------------------
procedure auto_adjust_btl();
begin
  if (iHandle = -1) then // 串口没打开
    Exit;

  wbuf[0] := AnsiChar('A');
  wbuf[1] := AnsiChar('T');
  wbuf[2] := COM_CMD_END_FLAG1; // 结束符
  wbuf[3] := COM_CMD_END_FLAG2;

  // 发送
  SP_Write(iHandle, wbuf, 0, 4);

  // 延时
  Sleep(10);
end;

// 启动IAP----------------------------------------------------------------------
function start_iap_cmd(sNo: AnsiString): boolean;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
  begin
    Exit;
  end;

  auto_adjust_btl(); // 自调整动速率

  wbuf[0] := #$2A; // 起始

  // wbuf[1] := #$51; // 机号
  // wbuf[2] := #$05;
  // wbuf[3] := #$07;
  wbuf[1] := TwoCtoOneC(sNo[1], sNo[2]); // 机号
  wbuf[2] := TwoCtoOneC(sNo[3], sNo[4]);
  wbuf[3] := TwoCtoOneC(sNo[5], sNo[6]);

  wbuf[4] := COM_CMD_IAP; // 命令

  wbuf[5] := #$00; // 数据长度

  wbuf[6] := GET_XOR(wbuf, 0, 5); // 异或校验

  wbuf[7] := COM_CMD_END_FLAG1; // 结束符
  wbuf[8] := COM_CMD_END_FLAG2;

  // 发送
  if (SP_Write(iHandle, wbuf, 0, 9) <> 9) then
  begin
    Exit;
  end;

  // 接收
  if (SP_Read(iHandle, rbuf, COM_CMD_MIN_SIZE, COM_READ_DELAY * 10) <>
    COM_CMD_MIN_SIZE) then // 读取失败
  begin
    Exit;
  end;

  Result := True;
end;

// 读固件版本-------------------------------------------------------------------
function get_soft_version(sNo: AnsiString; var ver: string): boolean;
var
  i: integer;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
    Exit;

  auto_adjust_btl(); // 自动调整速率

  wbuf[0] := #$2A; // 起始

  // wbuf[1] := #$51; // 机号
  // wbuf[2] := #$05;
  // wbuf[3] := #$07;
  wbuf[1] := TwoCtoOneC(sNo[1], sNo[2]); // 机号
  wbuf[2] := TwoCtoOneC(sNo[3], sNo[4]);
  wbuf[3] := TwoCtoOneC(sNo[5], sNo[6]);

  wbuf[4] := COM_CMD_SOFT_VERSION; // 命令

  wbuf[5] := #$00; // 数据长度

  wbuf[6] := GET_XOR(wbuf, 0, 5); // 异或校验

  wbuf[7] := COM_CMD_END_FLAG1; // 结束符
  wbuf[8] := COM_CMD_END_FLAG2;

  // 发送
  if (SP_Write(iHandle, wbuf, 0, 9) <> 9) then
    Exit;

  // 接收
  rdCount := SP_Read(iHandle, rbuf, COM_BUFF_SIZE, COM_READ_DELAY);
  if (rdCount <= COM_CMD_MIN_SIZE) then
    Exit;

  ver := '';
  for i := 0 to rdCount - COM_CMD_MIN_SIZE - 1 do
  begin
    ver := ver + rbuf[COM_CMD_DATA_IDX + i];
  end;
  ver := Trim(ver);

  Result := True;
end;

// 发送擦除命令-----------------------------------------------------------------
function iap_erase_cmd(sNo: AnsiString): boolean;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
  begin
    Exit;
  end;

  auto_adjust_btl(); // 自调整动速率

  wbuf[0] := #$2A; // 起始

  // wbuf[1] := #$51; // 机号
  // wbuf[2] := #$05;
  // wbuf[3] := #$07;
  wbuf[1] := TwoCtoOneC(sNo[1], sNo[2]); // 机号
  wbuf[2] := TwoCtoOneC(sNo[3], sNo[4]);
  wbuf[3] := TwoCtoOneC(sNo[5], sNo[6]);

  wbuf[4] := COM_CMD_ERASE_SECTOR; // 命令

  wbuf[5] := #$00; // 数据长度

  wbuf[6] := GET_XOR(wbuf, 0, 5); // 异或校验

  wbuf[7] := COM_CMD_END_FLAG1; // 结束符
  wbuf[8] := COM_CMD_END_FLAG2;

  // 发送
  if (SP_Write(iHandle, wbuf, 0, 9) <> 9) then
  begin
    Exit;
  end;

  // 接收
  if (SP_Read(iHandle, rbuf, COM_CMD_MIN_SIZE, COM_READ_DELAY * 30) <>
    COM_CMD_MIN_SIZE) then // 读取失败
  begin
    Exit;
  end;

  Result := True;
end;

// 下载数据---------------------------------------------------------------------
function down_data_cmd(sNo: AnsiString; A: array of AnsiChar;
  N: integer): boolean;
var
  i: integer;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
  begin
    Exit;
  end;

  // auto_adjust_btl(); // 自调整动速率

  wbuf[0] := #$2A; // 起始

  // wbuf[1] := #$51; // 机号
  // wbuf[2] := #$05;
  // wbuf[3] := #$07;
  wbuf[1] := TwoCtoOneC(sNo[1], sNo[2]); // 机号
  wbuf[2] := TwoCtoOneC(sNo[3], sNo[4]);
  wbuf[3] := TwoCtoOneC(sNo[5], sNo[6]);

  wbuf[4] := COM_CMD_IAP_DATA; // 命令

  wbuf[5] := AnsiChar(N + 2); // 数据长度

  for i := 0 to N - 1 do // 数据
  begin
    wbuf[i + 6] := A[i];
  end;

  Calculate_CRC(wbuf + 6, N);
  wbuf[COM_CMD_MIN_SIZE + N - 3] := crcHigh;
  wbuf[COM_CMD_MIN_SIZE + N - 2] := crcLow;

  wbuf[COM_CMD_MIN_SIZE + N - 1] := GET_XOR(wbuf, 0, COM_CMD_MIN_SIZE + N - 2);
  // 异或校验

  wbuf[COM_CMD_MIN_SIZE + N + 0] := COM_CMD_END_FLAG1; // 结束符
  wbuf[COM_CMD_MIN_SIZE + N + 1] := COM_CMD_END_FLAG2;

  // 发送
  if (SP_Write(iHandle, wbuf, 0, COM_CMD_MIN_SIZE + N + 2) <> COM_CMD_MIN_SIZE +
    N + 2) then
  begin
    Exit;
  end;

  // 接收
  if (SP_Read(iHandle, rbuf, COM_CMD_MIN_SIZE, COM_READ_DELAY * 10) <>
    COM_CMD_MIN_SIZE) then // 读取失败
  begin
    Exit;
  end;

  Result := True;
end;

// 启动App----------------------------------------------------------------------
function start_app_cmd(sNo: AnsiString): boolean;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
  begin
    Exit;
  end;

  // auto_adjust_btl(); // 自调整动速率

  wbuf[0] := #$2A; // 起始

  // wbuf[1] := #$51; // 机号
  // wbuf[2] := #$05;
  // wbuf[3] := #$07;
  wbuf[1] := TwoCtoOneC(sNo[1], sNo[2]); // 机号
  wbuf[2] := TwoCtoOneC(sNo[3], sNo[4]);
  wbuf[3] := TwoCtoOneC(sNo[5], sNo[6]);

  wbuf[4] := COM_CMD_END_DATA; // 命令

  wbuf[5] := #$00; // 数据长度

  wbuf[6] := GET_XOR(wbuf, 0, 5); // 异或校验

  wbuf[7] := COM_CMD_END_FLAG1; // 结束符
  wbuf[8] := COM_CMD_END_FLAG2;

  // 发送
  if (SP_Write(iHandle, wbuf, 0, 9) <> 9) then
  begin
    Exit;
  end;

  // 接收
  if (SP_Read(iHandle, rbuf, COM_CMD_MIN_SIZE, COM_READ_DELAY * 10) <>
    COM_CMD_MIN_SIZE) then // 读取失败
  begin
    Exit;
  end;

  Result := True;
end;

// 串口发送---------------------------------------------------------------------
function com_send(sValue: AnsiString; var rstC, rstS: AnsiString): boolean;
var
  i: integer;
  iCount: integer;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
  begin
    Exit;
  end;

  auto_adjust_btl(); // 自调整动速率

  iCount := Length(sValue) div 2;

  for i := 1 to iCount do
  begin
    wbuf[i - 1] := TwoCtoOneC(sValue[i * 2 - 1], sValue[i * 2]);
  end;

  // 发送
  if (SP_Write(iHandle, wbuf, 0, iCount) <> iCount) then
  begin
    Exit;
  end;

  // 接收
  iCount := SP_Read(iHandle, rbuf, 100, COM_READ_DELAY * 3);

  if iCount = 0 then
  begin
    Exit;
  end;

  rstC := '';
  rstS := '';
  for i := 0 to iCount - 1 do
  begin
    rstS := rstS + rbuf[i];
    rstC := rstC + OneCToTwoC(rbuf[i]) + ' ';
  end;
  rstC := Trim(rstC);

  Result := True;
end;

end.
