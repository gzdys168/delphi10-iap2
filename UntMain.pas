unit UntMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ComCtrls;

const
  SET_FILE = 'IAP.ini';
  SET_SECTION = 'IAP';
  COM_NO = 'ComNo';
  BAUD_RATE = 'BaudRate';
  IAP_FILE = 'IAPFile';
  Machine_No = 'MachineNo';
  Machine_Visible = 'MachineVisible';

type
  TfrmMain = class(TForm)
    pnlBottom: TPanel;
    pnlTop: TPanel;
    lbCOM: TLabel;
    lbBTL: TLabel;
    edtIAP: TEdit;
    btnDown: TButton;
    cbCOM: TComboBox;
    cbBTL: TComboBox;
    lbIAP: TLabel;
    btnIAP: TButton;
    pbIAP: TProgressBar;
    OpenDialog1: TOpenDialog;
    mmInfo: TMemo;
    btnClear: TButton;
    lbMachineNo: TLabel;
    edtMachineNo: TEdit;
    Label1: TLabel;
    edtTest: TEdit;
    btnTest: TButton;
    procedure FormCreate(Sender: TObject);
    procedure cbCOMChange(Sender: TObject);
    procedure cbBTLChange(Sender: TObject);
    procedure btnIAPClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    function DownBinFile(sNo: AnsiString): boolean;
  public

  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses DYSIni, UntCom, DYSChar, UntCMD, UntConst;

// 清除列表
procedure TfrmMain.btnClearClick(Sender: TObject);
begin
  mmInfo.Lines.Clear;
end;

// IAP（一键升级）
procedure TfrmMain.btnDownClick(Sender: TObject);
var
  sStr: string;
  sNo: AnsiString;
begin
  if not FileExists(edtIAP.Text) then
  begin
    edtIAP.Text := '';
    mmInfo.Lines.Add('请先选择下载文件！');
    Exit;
  end;

  sNo := Trim(edtMachineNo.Text);
  if Length(sNo) <> 6 then
  begin
    mmInfo.Lines.Add('请先填写6位机号！');
    Exit;
  end;

  try
    StrToInt('$' + sNo);
  except
    mmInfo.Lines.Add('机号只能包括[0,9],[a,f],[A,F]!');
    Exit;
  end;

  try
    Update;
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;

    btnDown.Enabled := False;
    open_port(cbCOM.Text, StrtoIntDef(cbBTL.Text, 115200));

    if (iHandle > 0) then // 打开串口成功
    begin
      if mmInfo.Lines.Count > 0 then
        mmInfo.Lines.Add('');
      mmInfo.Lines.Add('打开串口成功');

      // 启动IAP
      start_iap_cmd(sNo);
      // Sleep(1000);
      // Application.ProcessMessages;

      // -----------------------------------------------------------------------

      // 读取固件版本
      if get_soft_version(sNo, sStr) then
        mmInfo.Lines.Add('IAP版本：' + sStr)
      else
      begin
        mmInfo.Lines.Add('读取IAP版本失败！');
        Exit;
      end;
      Application.ProcessMessages;

      // 发送擦除命令
      if iap_erase_cmd(sNo) then
        mmInfo.Lines.Add('擦除成功')
      else
      begin
        mmInfo.Lines.Add('擦除失败！');
        Exit;
      end;
      Application.ProcessMessages;

      // 下载数据
      if DownBinFile(sNo) then
        mmInfo.Lines.Add('程序下载成功！')
      else
      begin
        mmInfo.Lines.Add('程序下载失败！');
        Exit;
      end;
      Application.ProcessMessages;

      // 启动App
      if start_app_cmd(sNo) then
        mmInfo.Lines.Add('启动应用程序成功')
      else
      begin
        mmInfo.Lines.Add('启动应用程序失败！');
        Exit;
      end;
      Application.ProcessMessages;

      mmInfo.Lines.Add('IAP（一键升级）完成！');
      iniWriteValue(AppPath + SET_FILE, SET_SECTION, Machine_Visible, sNo);
    end

    else // 打开串口失败
    begin
      if mmInfo.Lines.Count > 0 then
        mmInfo.Lines.Add('');
      mmInfo.Lines.Add('打开串口失败！');
    end;

  finally
    close_port();
    btnDown.Enabled := True;
    Screen.Cursor := crDefault;
  end;
end;

// 选择文件
procedure TfrmMain.btnIAPClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    edtIAP.Text := OpenDialog1.FileName;
    iniWriteValue(AppPath + SET_FILE, SET_SECTION, IAP_FILE,
      OpenDialog1.FileName);
  end;
end;

// 测试
procedure TfrmMain.btnTestClick(Sender: TObject);
var
  i: integer;
  sOut, sINC, sINS: AnsiString;
begin
  try
    Update;
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;

    btnTest.Enabled := False;
    open_port(cbCOM.Text, StrtoIntDef(cbBTL.Text, 115200));

    if (iHandle > 0) then // 打开串口成功
    begin
      sOut := Trim(edtTest.Text);
      sOut := StringReplace(sOut, ' ', '', [rfReplaceAll, rfIgnoreCase]); // 去空格
      if com_send(sOut, sINC, sINS) then
      begin
        if mmInfo.Lines.Count > 0 then
          mmInfo.Lines.Add('');
        mmInfo.Lines.Add(sINC);
        mmInfo.Lines.Add(sINS);
      end
      else
        mmInfo.Lines.Add('命令操作失败失败！');
    end
    else // 打开串口失败
    begin
      if mmInfo.Lines.Count > 0 then
        mmInfo.Lines.Add('');
      mmInfo.Lines.Add('打开串口失败！');
    end;
  finally
    close_port();
    btnTest.Enabled := True;
    Screen.Cursor := crDefault;
  end;
end;

// 修改波特率
procedure TfrmMain.cbBTLChange(Sender: TObject);
begin
  iniWriteValue(AppPath + SET_FILE, SET_SECTION, BAUD_RATE, cbBTL.Text);
end;

// 修改串口
procedure TfrmMain.cbCOMChange(Sender: TObject);
begin
  iniWriteValue(AppPath + SET_FILE, SET_SECTION, COM_NO, cbCOM.Text);
end;

// 下载文件
function TfrmMain.DownBinFile(sNo: AnsiString): boolean;
var
  size: Longint;
  FromF: file;
  NumRead: integer;
  Buf: array [1 .. 128] of AnsiChar;
begin
  Result := False;
  mmInfo.Lines.Add('开始下载...');

  try
    AssignFile(FromF, edtIAP.Text);
    Reset(FromF, 1);

    size := FileSize(FromF);
    pbIAP.Position := 0;
    pbIAP.Max := size div 128; // 1024 / 8 = 128

    repeat
      System.BlockRead(FromF, Buf, SizeOf(Buf), NumRead);
      if (NumRead > 0) then
      begin
        if not down_data_cmd(sNo, Buf, NumRead) then
        begin
          Exit;
        end;
        pbIAP.Position := pbIAP.Position + 1;
        Application.ProcessMessages;
      end;
    until (NumRead = 0);

    Result := True;
  finally
    CloseFile(FromF);
  end;
end;

// 窗体初始化
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  cbCOM.Text := iniReadValue(AppPath + SET_FILE, SET_SECTION, COM_NO, 'COM1');
  cbBTL.Text := iniReadValue(AppPath + SET_FILE, SET_SECTION, BAUD_RATE,
    '115200');
  edtIAP.Text := iniReadValue(AppPath + SET_FILE, SET_SECTION, IAP_FILE, '');

  edtMachineNo.Text := iniReadValue(AppPath + SET_FILE, SET_SECTION,
    Machine_Visible, '510507');
  lbMachineNo.Visible := iniReadValue(AppPath + SET_FILE, SET_SECTION,
    Machine_No, False);
  edtMachineNo.Visible := lbMachineNo.Visible;
end;

end.
